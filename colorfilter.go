package main

import (
	"image"
	"image/color"
	"image/draw"
)

// ColorFilter will overelay an image with a color filter of the specified color and opacity
func ColorFilter(original image.Image, filter color.Color) *image.NRGBA {

	dstimg := image.NewNRGBA(original.Bounds())

	fR, fG, fB, fA := filter.RGBA()

	mask := image.Uniform{color.Alpha{uint8(fA)}}

	filtercolor := color.RGBA{uint8(fR), uint8(fG), uint8(fB), 0xff}
	overlay := image.Uniform{filtercolor}

	draw.Draw(dstimg, dstimg.Bounds(), original, original.Bounds().Min, draw.Src)
	draw.DrawMask(dstimg, dstimg.Bounds(), &overlay, image.ZP, &mask, image.ZP, draw.Over)

	return dstimg
}
