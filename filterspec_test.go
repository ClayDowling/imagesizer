package main

import (
	"testing"
)

const FULLSATURATION = 255 * 0x101

func Test_FilterSpec_GivenEmptyString_ReturnsTransparent(t *testing.T) {
	_, _, _, a := FilterSpec("").RGBA()
	if a != 0 {
		t.Errorf("Expected alpha 0, got %v", a)
	}
}

func Test_FilterSpec_GivenFFF7_ReturnsWhiteWith7Alpha(t *testing.T) {
	r, g, b, a := FilterSpec("ffffff77").RGBA()
	if r != FULLSATURATION {
		t.Errorf("Expected full red, got %d", r)
	}
	if g != FULLSATURATION {
		t.Errorf("Expected full green, got %d", g)
	}
	if b != FULLSATURATION {
		t.Errorf("Expected full blue, got %d", b)
	}
	if a != 0x77*0x101 {
		t.Errorf("Expected %d, got %d", 0x77*0x101, a)
	}
}

func Test_FilterSpec_Given67f8_ReturnsSameAs6677ff88(t *testing.T) {
	r0, g0, b0, a0 := FilterSpec("6677ff88").RGBA()
	r1, g1, b1, a1 := FilterSpec("67f8").RGBA()
	if r0 != r1 {
		t.Errorf("Expected Red %d, got %d", r0, r1)
	}
	if g0 != g1 {
		t.Errorf("Expected Green %d, got %d", g0, g1)
	}
	if b0 != b1 {
		t.Errorf("Expected Blue %d, got %d", b0, b1)
	}
	if a0 != a1 {
		t.Errorf("Expected Alpha %d, got %d", a0, a1)
	}

}
