package main

import "image/color"

// FilterSpec give a hex code for a color will convert it to a
// color.
func FilterSpec(spec string) color.Color {
	filter := color.RGBA{0, 0, 0, 0}

	if len(spec) == 4 {
		newspec := string([]byte{spec[0], spec[0],
			spec[1], spec[1],
			spec[2], spec[2],
			spec[3], spec[3]})
		spec = newspec
	}

	if spec != "" {
		filter = color.RGBA{
			letterasint(spec[0])*0x10 + letterasint(spec[1]),
			letterasint(spec[2])*0x10 + letterasint(spec[3]),
			letterasint(spec[4])*0x10 + letterasint(spec[5]),
			letterasint(spec[6])*0x10 + letterasint(spec[7])}
	}

	return filter
}

func letterasint(letter byte) uint8 {
	switch letter {
	case '0':
		return 0
	case '1':
		return 1
	case '2':
		return 2
	case '3':
		return 3
	case '4':
		return 4
	case '5':
		return 5
	case '6':
		return 6
	case '7':
		return 7
	case '8':
		return 8
	case '9':
		return 9
	case 'a':
		return 10
	case 'A':
		return 10
	case 'b':
		return 11
	case 'B':
		return 11
	case 'c':
		return 12
	case 'C':
		return 12
	case 'd':
		return 13
	case 'D':
		return 13
	case 'e':
		return 14
	case 'E':
		return 14
	case 'f':
		return 15
	case 'F':
		return 15
	default:
		return 0
	}
}
