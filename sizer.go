package main

import (
	"encoding/json"
	"log"
	"os"
	"path/filepath"
)

// ImageSize is used to decode JSON files in folders to get default sizes.
type ImageSize struct {
	X      int
	Y      int
	Filter string
}

// GetFolderSize returns the ImageSize object declared for this folder.
func GetFolderSize(folder string) ImageSize {

	configfile := filepath.Join(folder, ".imagesizer")

	info, err := os.Stat(configfile)
	if err != nil {
		log.Print(err)
		return ImageSize{}
	}

	cfile, err := os.Open(configfile)
	if err != nil {
		log.Fatal(err)
	}
	defer cfile.Close()

	buf := make([]byte, info.Size())
	cfile.Read(buf)

	var size ImageSize

	json.Unmarshal(buf, &size)

	return size
}
