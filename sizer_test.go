package main

import (
	"testing"
)

func Test_GetFolderSize_GivenFolderWithGoodFile_ReturnsImageSize(t *testing.T) {
	actual := GetFolderSize("tests/good")
	if actual.X != 800 || actual.Y != 600 {
		t.Errorf("Expected {800 600}, got %v", actual)
	}
}

func Test_GetFolderSize_GivenFolderWithBadFile_ReturnsDefaultImageSizer(t *testing.T) {
	actual := GetFolderSize("tests/bad")
	if actual.X != 0 || actual.Y != 0 {
		t.Errorf("Expected {0 0}, got %v", actual)
	}
}

func Test_GetFolderSize_GivenNonExistantFile_ReturnsDefaultImageSizer(t *testing.T) {
	actual := GetFolderSize("tests/nonexistant")
	if actual.X != 0 || actual.Y != 0 {
		t.Errorf("Expected {0 0}, got %v", actual)
	}
}
