$env:OLDOS=$env:GOOS
$env:OLDARCH=$env:GOARCH

$env:GOOS="windows"
$env:GOARCH="amd64"
go build

$version = ./imagesizer.exe -v

$winfilename="imagesizer-$version-win.zip"
$osxfilename="imagesizer-$version-osx.zip"

zip $winfilename imagesizer.exe

$env:GOOS="darwin"
$env:GOARCH="amd64"
go build
zip $osxfilename imagesizer

$env:GOOS=$env:OLDOS
$env:GOARCH=$env:OLDARCH
