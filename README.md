# ImageSizer

A tool for automatically sizing images for a website.

Assuming a setup where each image asset folder holds images of a certain size, this tool will automatically size images correctly for each folder.  Images will be scaled down to the appropriate size, then cropped to fit into the correct bounding box.  The images will be cropped from the center of the photo.

## Installing

1. Download the latest binary for your operating system

    * [OSX](http://claydowling.com/software/imagesizer-1.3-osx.zip)
    * [Windows](http://claydowling.com/software/imagesizer-1.3-win.zip)

1. Extract the executable from the .zip file
1. Copy the executable to someplace in your path.

## Configuration

If you have the following structure:

    /assets
      /image
        /wide  1600x600
        /card  800x600

In each folder which will hold images, put a file `.imagesizer`.

The file is a json file like this:

    {
        "X": 800,
        "Y": 600,
        "Filter": "00274c88" // University of Michigan Blue
    }

## Running Imagesizer

Let's say that you've downloaded your latest photographic masterpieces from your camera into `~/Pictures`.

From the command line, run

    imagesizer -dest /path/to/my/blog/assets/image/wide photo1.jpg photo2.jpg photo3.jpg

In your blog, the following new files will be available:

    /assets
      /image
        /wide  1600x600
          photo1.jpg
          photo2.jpg
          photo3.jpg

        /card  800x600

The new images will be scaled and cropped to the 1600x600 size.

## Color Filters

For presentations you may want a certain color theme.  For instance you might want background images in a presentation to be seen through a blue filter.  A new "Filter" option has been added to the command line, and to the `.imagesizer` file.  This property is optional in both cases.

Consider this starting image:

![Garter Snake](garter-snake-port-crescent.JPG)

If it were supplied as a source image to imagesizer, and given the `tests/good` destination path, it would wind up like this:

![Wolverine Garter Snake](tests/good/garter-snake-port-crescent.JPG)

### Color Filter Values

The color filter values must be in one of two forms:

* rrggbbaa

* rgba

The alpha value determines the intensity of the overlay.  So '5' is a hint of the color, with a little of the original color showing through, while 'a' is more or less a monochromatic image.

### Color Considerations

If you would rather not have any of the original color show through, you might be better served to desaturate the original image, producing a black and white shot.  This will produce a purely monochromatic image even if you have a fairly low alpha value.