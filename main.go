package main

import (
	"flag"
	"fmt"
	"image"
	"image/color"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"log"
	"os"
	"path"
	"path/filepath"

	"github.com/disintegration/imaging"
)

var majorVersion = 1
var minorVersion = 4

func main() {

	var destfolder string
	var filterSpec string
	var filter color.Color
	flag.StringVar(&destfolder, "dest", ".", "Destination folder, which contains .imagesizer file")
	flag.StringVar(&filterSpec, "filter", "", "Color filter, of the form RGBA")
	showMinimalVersion := flag.Bool("v", false, "Display minimal version and exit")
	showVersion := flag.Bool("version", false, "Display version and exit")

	flag.Parse()

	if *showVersion == true {
		fmt.Printf("ImageSizer version %d.%d\n", majorVersion, minorVersion)
		return
	}

	if *showMinimalVersion == true {
		fmt.Printf("%d.%d\n", majorVersion, minorVersion)
		return
	}

	if filterSpec != "" {
		filter = FilterSpec(filterSpec)
	}

	targetsize := GetFolderSize(destfolder)
	if targetsize.X == 0 || targetsize.Y == 0 {
		log.Fatalf("No valid .imagesizer file found in %s", destfolder)
	}
	if filterSpec == "" {
		if targetsize.Filter != "" {
			filter = FilterSpec(targetsize.Filter)
		}
	}

	for _, filename := range flag.Args() {
		img := openImage(filename)
		scaled := imaging.Fill(img, targetsize.X, targetsize.Y, imaging.Center, imaging.Linear)

		if filterSpec != "" {
			_, _, _, A := filter.RGBA()
			if A > 0 {
				scaled = ColorFilter(scaled, filter)
			}
		}

		writer, err := os.Create(filepath.Join(destfolder, path.Base(filename)))
		if err != nil {
			log.Fatal(err)
		}
		imaging.Encode(writer, scaled, imaging.JPEG)
		writer.Close()
	}
}

func openImage(filename string) image.Image {
	reader, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer reader.Close()
	img, _, err := image.Decode(reader)
	if err != nil {
		log.Fatal(err)
	}
	return img
}
